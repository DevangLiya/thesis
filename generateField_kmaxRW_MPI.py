#Author: Devang H. Liya
#This program generates a gaussian random field with given box size in given dimentions.
#It then perfroms random walks with different k_max and samples field at every point in the box at every step.
#Finally, the program saves two numpy arrays
#1. shape (N**dim, int(N/2)) array containing random walks for N**dim points with k_max in decreasing order.
#2. shape (int(N/2),) array with k_max values in decreasing order.

#Set warnings to show always
import warnings
warnings.filterwarnings('always')

import numpy as np
import matplotlib.pyplot as plt
import pyfftw
import scipy.stats as stats
from scipy.stats import moment

from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

#----------------------------------------------------------------------
# Field generation and visualization functions
#----------------------------------------------------------------------


def p(k_vec, dim, p_pow=0):
	'''
	Return power spectrum evaluated at given k-values.
	Parameters:
	----
	k_vec: Vector of k-values as numpy array of size (N,1). This follows the convention 2*np.pi*np.fft.fftfreq(N)
	dim: Dimention of the field. Possible values are 1, 2, 3.
	p_pow: (default=0) Spectral power of the power spectrum.
	
	Return :
	----
	Power spectrum evaluated on a given grid.
	'''
	
	if dim == 1:
		k_mesh = np.meshgrid(k_vec, sparse=True)
	elif dim == 2:
		k_mesh = np.meshgrid(k_vec, k_vec, sparse=True)
	elif dim == 3:
		k_mesh = np.meshgrid(k_vec, k_vec, k_vec, sparse=True)
		
	ps_grid = k_mesh[0]**2
	for kaxis in k_mesh[1:]:
		ps_grid = ps_grid + kaxis**2
		
	ps_grid = np.sqrt(ps_grid)
	ps_eval = np.power(ps_grid, p_pow)
	ps_eval[ps_eval == np.inf] = 0
	
	return ps_eval        
			
def generate_mask(N, dim, n_remove):
	'''
	Generate a mask to apply a filter based on frequency selection.
	
	Parameters:
	----
	N: number of intervals on each axis
	dim: Dimention of the field. Possible values are 1, 2, 3.
	n_remove (<= int(N/2)): Number of frequencies to remove from the high-k end i.e. decrease the k_max or say 
	that fluctuations at smaller scales are not important.
	
	Return:
	----
	None if n_remove > int(N/2)
	A numpy array containing 0 for frequencies that are to be removed and 1 for all other frequencies.
	This array has shape (N,)/(N,N)/(N,N,N) depending on the value of dim. 
	'''
	
	if n_remove > int(N/2):
		print("More frequencies to remove than existing")
		return None
	amp_mask = np.ones(shape=[N for i in range(dim)])
	
	if n_remove == 0: return amp_mask
	
	if dim == 1:
		amp_mask[int(N/2) - n_remove:int(N/2) + n_remove + 1] = 0
	elif dim == 2:
		amp_mask[int(N/2) - n_remove:int(N/2) + n_remove + 1, int(N/2) - n_remove:int(N/2) + n_remove + 1] = 0
	elif dim == 3:
		amp_mask[int(N/2) - n_remove:int(N/2) + n_remove + 1, int(N/2) - n_remove:int(N/2) + n_remove + 1,
				int(N/2) - n_remove:int(N/2) + n_remove + 1] = 0
	
	return amp_mask

def get_amplitude(N, dim, L, p_pow, random_seed=None):
	'''
	Evaluate G(k) by multiplying the white noise to the square root of power spectrum.
	
	Parameters:
	----
	N: Size of the box.
	dim: Dimention of the field. Possible values are 1, 2, 3.
	L: Seperation between points along axes in the box.
	p_pow: Spectral power of the power spectrum.
	random_seed: (default=None) Value of seed that is used to generate white noise.
	
	Return:
	----
	A numpy array containing G(k) evaluated on the given grid.
	This array has shape (N,)/(N,N)/(N,N,N) depending on the value of dim.
	'''
	
	if random_seed is not None:
		np.random.seed(random_seed)
	
	k_vec = 2*np.pi*np.fft.fftfreq(N)

	random_field = np.fft.fftn(np.random.normal(size=[N for i in range(dim)]))

	ps_eval = p(k_vec, dim, p_pow)
	ps_eval = np.sqrt(ps_eval)
	amplitude = random_field*ps_eval
	
	return amplitude

def sanity_check_field(field):
	'''
	A function to print simple diagnostic information for the given field.
	Parameter:
	----
	field: numpy array with complex values.
	
	Return:
	----
	Print following information: 
	1. Average and mean of the ratio of the real and imaginary parts of the field.
	Both these numbers should be close to 0 if the field is generated correctly.
	2. Moments 1 to 5 of the flattened real part of the field. Odd moments should be close to zero if the 
	field is generated correctly.
	
	Returns 0 if executed properly.
	'''
	
	print("Printing diganostic information for sanity check:")
	
	real_field = np.real(field)
	imaginary_field = np.imag(field)
		
	im_to_re_ratio = imaginary_field/real_field
	avg_ratio = np.mean(im_to_re_ratio)
	std_ratio = np.std(im_to_re_ratio)
	print("Imaginary to real ratio: Average= %e; Standard deviation= %e"%(avg_ratio, std_ratio))
	if (std_ratio < 1e-10) and (avg_ratio < 1e-10): print("These ratios look OK!")
	else: print("Something wrong with the ratios!")

	for n_moment in range(1,6):
		print("Moment " + str(n_moment) + " of the real part of the field = %e"%(moment(real_field.flatten(),
																			moment=n_moment)))
	
	return 0
	
def plot_flattened_field(field, sanity_check=True):
	'''
	Plot a histogram of the flattened real part of the field.\
	Paramaters:
	----
	field: numpy array with complex values.
	sanity_check: (default=True) Call the function sanity_check_field to print diagnostic information.
	
	Return:
	----
	Returns zero if executed correctly
	
	To-do:
	----
	Take axis object and return that axis object.
	'''
	real_field = np.real(field)
	imaginary_field = np.imag(field)
	plt.figure()
	plt.hist(real_field.flatten(), bins=50)
	
	if sanity_check:
		sanity_check_field(field)
		
	return 0

def sample_field(sample_points, real_field, dim):
	'''
	Return the value of field at given points. Useful while performing random walks.
	
	Parameters:
	----
	sample_points: A list of co-ordinates where the field is to be sampled.
	real_field: A numpy array with real values representing the field.
	dim: Dimention of the field. Possible values are 1, 2, 3. 
	'''
	rwl = np.zeros(shape=(len(sample_points)))
	for n_sp, sample_point in enumerate(sample_points):
		if dim == 1:
			rwl[n_sp] = real_field[sample_point[0]]
		elif dim == 2:
			rwl[n_sp] = real_field[sample_point[0], sample_point[1]]
		elif dim == 3:
			rwl[n_sp] = real_field[sample_point[0], sample_point[1], sample_point[2]]
	
	return rwl


#----------------------------------------------------------------------
# Start of program
#----------------------------------------------------------------------

dim = 3
N = 128
L = 1
p_pow = -2
random_seed = 42
del_k = 2*np.pi/(N*L)

amplitude = get_amplitude(N=N, dim=dim, L=L, p_pow=p_pow, random_seed=random_seed)
k_max_list = []

if rank == 0:
	sample_points = []
	for i in range(N):
		for j in range(N):
			for k in range(N):
				sample_points.append([i,j,k])

	chunks = [[] for _ in range(size)]
	for i, chunk in enumerate(sample_points):
		chunks[i % size].append(chunk)

else:
	sample_points = None
	chunks = None

sample_points = comm.scatter(chunks, root=0)
rw = np.zeros(shape=(len(sample_points), len(range(int(N/2) + 1))))

for n_remove in range(int(N/2) + 1):
	amp_mask = generate_mask(N, dim, n_remove)
	k_max = del_k*((N/2) - 1 - n_remove)
	k_max_list.append(k_max)
	masked_amplitude = amp_mask*amplitude
	field = np.fft.fftn(masked_amplitude)/(N*L)
	real_field = np.real(field)

	rw[:, n_remove] = sample_field(sample_points, real_field, dim)

comm.Barrier()
newData = comm.gather(rw ,root=0)

if rank == 0:
	k_max_list[-1] = 0
	final_rw = np.vstack(newData)
	print(final_rw.shape)
	np.save('rw' + str(N) + '_' + str(dim) + 'D.npy', final_rw)
	np.save('k_max_list' + str(N) + '_' + str(dim) + 'D.npy', np.array(k_max_list))
