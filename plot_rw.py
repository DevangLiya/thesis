import numpy as np
import pdb
from tqdm import tqdm
import matplotlib.pyplot as plt

#----------------------------------------------------------------------
# Plot decoration function
#----------------------------------------------------------------------
#Decorate plots
import matplotlib.ticker as ticker

def tick_marks(ax, x1=0, x2=0, nx=0, dx=1, y1=0, y2=0, ny=0, dy=0, majortick_size=15, minortick_size=8, 
			   majortick_width=2, minortick_width=1.5, tick_label_size=18, spwid=2):
	plt.sca(ax)
	#setting tickmarks
	if(nx>0):
		minorLocator_x   = ticker.MultipleLocator((x2-x1-dx)/(nx*5.0))
		ax.xaxis.set_minor_locator(minorLocator_x)
		#dx=(x2-x1)/nx/5
		plt.xticks(np.round(np.linspace(x1+dx,x2,nx),2),fontsize=18)
		plt.xlim([x1,x2])
	elif(nx==-1):
		plt.xticks([])
	else:
		xscale=ax.get_xscale()
		majorloc_x=ax.get_xticks()
		if('linear' in xscale):
			minorloc_x   = ticker.MultipleLocator((majorloc_x[-1]-majorloc_x[0])/((majorloc_x.size-1)*5.0))
			ax.xaxis.set_minor_locator(minorloc_x)
		if('log' in xscale):
			minorloc_x=[majorloc_x[0]]
			for mm in majorloc_x[:-1]:
				for jj in range(1,10):
					minorloc_x.append(minorloc_x[-1]+mm)
			ax.xaxis.set_minor_locator(ticker.FixedLocator(minorloc_x))
			ax.xaxis.set_minor_formatter(plt.NullFormatter())
		

	if(ny>0):
		minorLocator_y   = ticker.MultipleLocator((y2-y1-dy)/(ny*5.0))
		ax.yaxis.set_minor_locator(minorLocator_y)
		plt.yticks(np.round(np.linspace(y1+dy,y2,ny),2),fontsize=20)
		plt.ylim([y1,y2])
	elif(ny==-1):
		plt.yticks([]) 
	else:
		yscale=ax.get_yscale()
		majorloc_y=ax.get_yticks()
		if('linear' in yscale):
			minorloc_y   = ticker.MultipleLocator((majorloc_y[-1]-majorloc_y[0])/((majorloc_y.size-1)*5.0))
			ax.yaxis.set_minor_locator(minorloc_y)
		if('log' in yscale):
			minorloc_y=[majorloc_y[0]]
			for mm in majorloc_y[:-1]:
				for jj in range(1,10):
					minorloc_y.append(minorloc_y[-1]+mm)
			ax.yaxis.set_minor_locator(ticker.FixedLocator(minorloc_y))
			ax.yaxis.set_minor_formatter(plt.NullFormatter())

	
	plt.tick_params('both', length=majortick_size, width=majortick_width, which='major', 
					labelsize=tick_label_size,direction='inout')
	plt.tick_params('both', length=minortick_size, width=minortick_width, which='minor',
					labelsize=tick_label_size,direction='inout')

	#ax=fig.add_subplot(111)
	for axis in ['top','bottom','left','right']:
		ax.spines[axis].set_linewidth(spwid)

	return 

N = 64
dim = 2
rw = np.load('rw' + str(N) + '_' + str(dim) + 'D.npy')
k_max_list = np.load('k_max_list' + str(N) + '_' + str(dim) + 'D.npy')

fig, ax = plt.subplots(1, 1, sharex=False, sharey=False, figsize=(8,6))

for i in tqdm(range(rw.shape[0])):
	plt.plot(k_max_list, rw[i], lw=0.5, color='gray')
plt.xlabel(r"k$_{max}$", fontsize=22)
plt.ylabel(r"f(x, k$_{max}$)", fontsize=22)
tick_marks(ax, x1=0, x2=0, nx=0, dx=1, y1=0, y2=0, ny=0, dy=0, spwid=2, majortick_size=10, minortick_size=6, majortick_width=1.5, 
	minortick_width=1, tick_label_size=14)
	
plt.savefig('rw' + str(N) + '_' + str(dim) + 'D.png', dpi=300, bbox_inches='tight')
